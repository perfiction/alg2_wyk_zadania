﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace GraphColoringComparison
{
    public partial class RandomGraphForm : Form
    {
        private Graph _output;
        public Graph Output => _output;

        private Dictionary<string, PointF> _locations;
        public Dictionary<string, PointF> Locations => _locations;

        public RandomGraphForm()
        {
            InitializeComponent();
        }

        private void vertexCountNud_ValueChanged(object sender, EventArgs e)
        {
            int v = (int)vertexCountNud.Value;
            edgeCountNud.Maximum = v * (v - 1) / 2;
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            int vertCount = (int)vertexCountNud.Value;
            int edgeCount = (int)edgeCountNud.Value;
            Random rand = new Random();

            _locations = new Dictionary<string, PointF>(vertCount);
            _output = new Graph();

            for (int i = 0; i < vertCount; i++)
            {
                _output.AddVertex(i.ToString());

                float posX = (float)rand.NextDouble();
                float posY = (float)rand.NextDouble();
                _locations.Add(i.ToString(), new PointF(posX, posY));
            }

            List<string> vertices = _output.GetAllVertices();
            for (int i = 0; i < edgeCount; i++)
            {
                string from, to;
                do
                {
                    from = vertices[rand.Next(vertices.Count)];
                    do
                    {
                        to = vertices[rand.Next(vertices.Count)];
                    } while (to == from);
                } while (_output.EdgeExists(from, to));

                _output.AddEdge(from, to);
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
