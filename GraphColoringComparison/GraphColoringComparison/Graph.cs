﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphColoringComparison
{
    public class Graph : IDisposable
    {
        private Dictionary<string, List<string>> _graph;

        public int VertCount => _graph.Count;
        public int EdgeCount => CalculateEdges();

        public Graph()
        {
            _graph = new Dictionary<string, List<string>>();
        }

        public void AddVertex(string v)
        {
            if (!_graph.ContainsKey(v) || _graph[v] == null)
                _graph.Add(v, new List<string>());
        }

        public void RemoveVerted(string v)
        {
            if (_graph.ContainsKey(v) && _graph[v] != null)
            {
                _graph[v].Clear();
                _graph[v] = null;
            }

            foreach (string vertex in _graph.Keys)
            {
                if (_graph[vertex] != null && _graph[vertex].Contains(v))
                {
                    _graph[vertex].Remove(v);
                }
            }

            _graph.Remove(v);
        }

        public void AddEdge(string from, string to)
        {
            if (!_graph.ContainsKey(from) || _graph[from] == null) _graph[from] = new List<string>();
            if (!_graph[from].Contains(to)) _graph[from].Add(to);

            if (!_graph.ContainsKey(to) || _graph[to] == null) _graph[to] = new List<string>();
            if (!_graph[to].Contains(from)) _graph[to].Add(from);
        }

        public void RemoveEdge(string from, string to)
        {
            if (_graph.ContainsKey(from) && _graph[from] != null)
                _graph[from].Remove(to);

            if (_graph.ContainsKey(to) && _graph[to] != null)
                _graph[to].Remove(from);
        }

        public bool EdgeExists(string from, string to)
        {
            return _graph.ContainsKey(from) && _graph[from] != null && _graph[from].Contains(to);
        }

        public List<string> GetAllVertices()
        {
            return _graph.Keys.ToList();
        }

        public List<(string from, string to)> GetAllEdges()
        {
            List<(string from, string to)> edges = new List<(string, string)>();

            foreach (string from in _graph.Keys)
                foreach (string to in _graph[from])
                    edges.Add((from, to));

            return edges;
        }

        public bool HasNeighbors(string v)
        {
            return _graph.ContainsKey(v) && _graph[v] != null && _graph.Count != 0;
        }

        public List<string> GetNeighbors(string v)
        {
            return _graph.ContainsKey(v) && _graph[v] != null ? _graph[v] : new List<string>();
        }

        private int CalculateEdges()
        {
            int edges = 0;
            List<(string, string)> analyzedEdges = new List<(string, string)>();

            foreach (string from in _graph.Keys)
            {
                foreach (string to in _graph[from])
                {
                    if (analyzedEdges.Contains((from, to)) || analyzedEdges.Contains((to, from))) continue;

                    analyzedEdges.Add((from, to));
                    edges++;
                }
            }

            return edges;
        }

        public int CalculateDegree(string v)
        {
            return _graph.ContainsKey(v) ? _graph[v].Count : 0;
        }

        public void Dispose()
        {
            foreach (string key in _graph.Keys)
            {
                _graph[key].Clear();
                _graph[key] = null;
            }

            _graph.Clear();
            _graph = null;
        }
    }
}
