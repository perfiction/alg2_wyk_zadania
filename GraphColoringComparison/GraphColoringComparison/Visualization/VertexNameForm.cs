﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GraphColoringComparison.Visualization
{
    public partial class VertexNameForm : Form
    {
        public string VertexName => tbVertexName.Text;
        private readonly List<string> _currentVertices;

        public VertexNameForm(List<string> currentVertices)
        {
            InitializeComponent();
            _currentVertices = currentVertices;

            errorProvider.SetError(tbVertexName, "Vertex name cannot be empty");
            btnOk.Enabled = false;
        }

        private void tbVertexName_TextChanged(object sender, EventArgs e)
        {
            if (_currentVertices.Contains(tbVertexName.Text))
            {
                btnOk.Enabled = false;
                errorProvider.SetError(tbVertexName, "Vertex with this name already exists");
            } else if (tbVertexName.Text.Contains(";"))
            {
                btnOk.Enabled = false;
                errorProvider.SetError(tbVertexName, "Vertex name cannot contain semicolon (;)\nIt's used as separator in graph files");
            }
            else
            {
                btnOk.Enabled = true;
                errorProvider.Clear();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
