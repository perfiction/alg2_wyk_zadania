﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace GraphColoringComparison.Visualization
{
    public partial class GraphView : UserControl
    {
        #region Zmienne do rysowania (rozmiary / kolory)

        private int _vertexSize = 60;
        private const int VertexBorderWidth = 2;
        private const int EdgeWidth = 2;
        private readonly Pen _vertexBorderPen = new Pen(Color.Black, VertexBorderWidth);
        private readonly Pen _edgePen = new Pen(Color.Black, EdgeWidth);

        private Dictionary<string, Color> _colorMap;

        #endregion

        #region Zmienne do logiki 

        private Dictionary<string, PointF> _vertexLocations;

        public Dictionary<string, PointF> VertexLocations
        {
            get => _vertexLocations;
            set
            {
                if (value != null && value.Count != 0)
                {
                    int bigGraphPointSize = Math.Min(Width, Height) / value.Count;
                    if (bigGraphPointSize < 5) _vertexSize = bigGraphPointSize * 5;
                }

                _vertexLocations = value;
            }
        }

        private bool _edgeModeEnabled;
        private string _edgeFrom, _edgeTo;
        private Point _cursorPoint;

        #endregion

        private Graph _g;

        [Description("Enable/Disable visualization mode (disable mouse events and enable vertex coloring)"), Category("Appearance")]
        public bool PresentationMode { get; set; }

        private bool _antialiasing;
        [Description("Enable/Disable anti-aliasing (can drastically slow down the application)"), Category("Appearance")]
        public bool Antialiasing
        {
            get => _antialiasing;
            set
            {
                _antialiasing = value;
                Invalidate();
            }
        }

        public GraphView()
        {
            InitializeComponent();

            //Włącz podwójne buforowanie, by zapobiec miganiu kontrolki podczas Invalidate: https://stackoverflow.com/a/15815254
            typeof(UserControl).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic, null, this, new object[] { true });

            VertexLocations = new Dictionary<string, PointF>();
        }

        public void SetGraph(Graph g)
        {
            _g = g;
        }

        public void SetColors(Dictionary<string, int> colors)
        {
            _colorMap = GenerateColorMap(colors);
        }

        #region Logika (eventy myszki)

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (PresentationMode) return;

            base.OnMouseClick(e);

            Point clickPoint = new Point(e.X, e.Y);
            switch (e.Button)
            {
                // W trakcie dodawania krawedzi
                case MouseButtons.Left when _edgeModeEnabled:
                    {
                        string clickedVertex = GetClickedVertex(clickPoint);
                        if (clickedVertex == null) return;

                        // Kliknieto drugi wierzcholek - utworz krawedz
                        _edgeTo = clickedVertex;
                        if (_edgeFrom == _edgeTo || _g.EdgeExists(_edgeFrom, _edgeTo)) return;

                        _g.AddEdge(_edgeFrom, _edgeTo);
                        _edgeFrom = _edgeTo = null;
                        _edgeModeEnabled = false;
                        Invalidate();
                        break;
                    }
                // Dodajemy wierzcholek lub rozpoczynamy dodawanie krawdzi
                case MouseButtons.Left:
                    {
                        string clickedVertex = GetClickedVertex(clickPoint);
                        bool intersects = clickedVertex != null;

                        if (!_edgeModeEnabled && intersects && _g.VertCount >= 2) // Kliknieto istniejacy wierzcholek - rozpocznij dodawanie krawedzi
                        {
                            _edgeModeEnabled = true;
                            _edgeFrom = clickedVertex;
                        }
                        else if (!_edgeModeEnabled && !intersects) // Klikineto pusta przestrzen - dodaj nowy wierzcholek
                        {
                            VertexNameForm form = new VertexNameForm(VertexLocations.Keys.ToList());
                            form.StartPosition = FormStartPosition.CenterParent;
                            DialogResult result = form.ShowDialog(this);
                            if (result != DialogResult.OK) return;

                            string newVertexName = form.VertexName;
                            _g.AddVertex(newVertexName);
                            VertexLocations[newVertexName] = new PointF(e.X / (float)Width, e.Y / (float)Height);
                            Invalidate();
                        }

                        break;
                    }
                // W trakcie dodawania krawedzi - wylacz tryb dodawania krawedzi
                case MouseButtons.Right when _edgeModeEnabled:
                    {
                        _edgeModeEnabled = false;
                        Invalidate();
                        return;
                    }
                case MouseButtons.Right:
                    {
                        string clickedVertex = GetClickedVertex(clickPoint);
                        if (clickedVertex != null) // Kliknieto wierzcholek - usun go
                        {
                            _g.RemoveVerted(clickedVertex);
                            VertexLocations.Remove(clickedVertex);
                            Invalidate();
                            return;
                        }

                        (string clickedEdgeFrom, string clickedEdgeTo) = GetClickedEdge(clickPoint);
                        if (clickedEdgeFrom == null || clickedEdgeTo == null) return;

                        // Kliknieto w krawedz - usun ja
                        _g.RemoveEdge(clickedEdgeFrom, clickedEdgeTo);
                        Invalidate();
                        break;
                    }
                case MouseButtons.None:
                case MouseButtons.Middle:
                case MouseButtons.XButton1:
                case MouseButtons.XButton2:
                    return;
                default: return;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (PresentationMode || !_edgeModeEnabled) return;

            _cursorPoint = new Point(e.X, e.Y);
            Invalidate();
        }

        #endregion

        #region Funkcje pomocnicze do logiki

        private string GetClickedVertex(Point clickPoint)
        {
            foreach (string vertex in VertexLocations.Keys)
            {
                float x = VertexLocations[vertex].X * Width - _vertexSize / 2f;
                float y = VertexLocations[vertex].Y * Height - _vertexSize / 2f;
                Rectangle vertexRect = new Rectangle((int)x, (int)y, _vertexSize, _vertexSize);
                if (RectContainsPoint(clickPoint, vertexRect))
                    return vertex;
            }

            return null;
        }

        private (string from, string to) GetClickedEdge(Point clickPoint)
        {
            foreach ((string from, string to) in _g.GetAllEdges())
            {
                Point p1 = new Point((int)(VertexLocations[from].X * Width), (int)(VertexLocations[from].Y * Height));
                Point p2 = new Point((int)(VertexLocations[to].X * Width), (int)(VertexLocations[to].Y * Height));
                if (LineContainsPoint(clickPoint, p1, p2))
                    return (from, to);
            }

            return (null, null);
        }

        private static bool RectContainsPoint(Point p, Rectangle r)
        {
            return r.Contains(p);
        }

        private static bool LineContainsPoint(Point p, Point a, Point b)
        {
            double ab = Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
            double ap = Math.Sqrt(Math.Pow(a.X - p.X, 2) + Math.Pow(a.Y - p.Y, 2));
            double pb = Math.Sqrt(Math.Pow(p.X - b.X, 2) + Math.Pow(p.Y - b.Y, 2));

            //return ap + pb == ab;
            return Math.Abs(ap + pb - ab) < 0.1;
        }

        #endregion

        #region Rysowanie

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            _vertexSize = Math.Min(Width, Height) / 10;

            if (VertexLocations != null && VertexLocations.Count > 0)
            {
                int bigGraphPointSize = Math.Min(Width, Height) / VertexLocations.Count;
                if (bigGraphPointSize < 5) _vertexSize = bigGraphPointSize * 5;
            }

            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.SmoothingMode = _antialiasing ? SmoothingMode.HighQuality : SmoothingMode.None;

            if (_edgeModeEnabled)
                DrawEdgeMode(e.Graphics);

            DrawEdges(e.Graphics);

            if (!PresentationMode)
                DrawVertices(e.Graphics);
            else if (_colorMap != null)
                DrawVertices(e.Graphics, true);

        }

        private void DrawEdgeMode(Graphics g)
        {
            if (_cursorPoint.X == 0 && _cursorPoint.Y == 0) return;

            Point vertexPoint = new Point((int)(VertexLocations[_edgeFrom].X * Width), (int)(VertexLocations[_edgeFrom].Y * Height));
            g.DrawLine(_edgePen, vertexPoint, _cursorPoint);
        }

        private void DrawVertices(Graphics g, bool useColorMap = false)
        {
            if (_g == null) return;

            foreach (string vertex in _g.GetAllVertices())
            {
                float x = VertexLocations[vertex].X * Width - _vertexSize / 2f;
                float y = VertexLocations[vertex].Y * Height - _vertexSize / 2f;
                Rectangle vertexRect = new Rectangle((int)x, (int)y, _vertexSize, _vertexSize);
                g.FillEllipse(useColorMap == false ? new SolidBrush(BackColor) : new SolidBrush(_colorMap[vertex]), vertexRect);
                g.DrawEllipse(_vertexBorderPen, vertexRect);
                (Font font, SizeF textSize) = AdjustFont(g, vertex, vertexRect, 24);
                if (font != null)
                    g.DrawString(vertex, font, Brushes.Black, vertexRect.X + (vertexRect.Width - textSize.Width) / 2, vertexRect.Y + (vertexRect.Height - textSize.Height) / 2);
            }
        }

        private void DrawEdges(Graphics g)
        {
            if (_g == null) return;

            foreach ((string from, string to) in _g.GetAllEdges())
            {
                Point p1 = new Point((int)(VertexLocations[from].X * Width), (int)(VertexLocations[from].Y * Height));
                Point p2 = new Point((int)(VertexLocations[to].X * Width), (int)(VertexLocations[to].Y * Height));
                g.DrawLine(_edgePen, p1, p2);
            }
        }

        private static (Font font, SizeF textSize) AdjustFont(Graphics g, string text, Rectangle container, int baseFontSize)
        {
            //Rectangle containerWithPadding = new Rectangle(container.X, container.Y, (int)(container.Width * 0.95), (int)(container.Height * 0.95));
            for (int i = baseFontSize; i >= 1; i--)
            {
                Font font = new Font("Arial", i, FontStyle.Bold);
                SizeF textSize = g.MeasureString(text, font);

                if (container.Width > textSize.Width && container.Height > textSize.Height)
                    return (font, textSize);
            }

            return (null, SizeF.Empty);
        }

        private static Dictionary<string, Color> GenerateColorMap(Dictionary<string, int> colorNumbers)
        {
            if (colorNumbers.Count == 0) return new Dictionary<string, Color>();

            Dictionary<string, Color> colorMap = new Dictionary<string, Color>();

            int colorCount = colorNumbers.Select(x => x.Value).Distinct().Count();
            Color[] usedColors = GetColors(colorCount);

            foreach (string vertex in colorNumbers.Keys)
            {
                colorMap.Add(vertex, usedColors[colorNumbers[vertex]]);
            }

            return colorMap;
        }

        private static Color[] GetColors(int count)
        {
            Color[] colors = new Color[count];

            int step = 360 / count;
            for (int i = 0; i < count; i++)
            {
                float h = i * step;
                colors[i] = ColorFromHsv(h, 1, 1);
            }

            return colors;
        }

        private static Color ColorFromHsv(float hue, float saturation, float value)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            value *= 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            switch (hi)
            {
                case 0:
                    return Color.FromArgb(255, v, t, p);
                case 1:
                    return Color.FromArgb(255, q, v, p);
                case 2:
                    return Color.FromArgb(255, p, v, t);
                case 3:
                    return Color.FromArgb(255, p, q, v);
                case 4:
                    return Color.FromArgb(255, t, p, v);
                default:
                    return Color.FromArgb(255, v, p, q);
            }
        }

        #endregion
    }
}
