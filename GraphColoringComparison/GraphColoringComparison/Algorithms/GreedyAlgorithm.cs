﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;

namespace GraphColoringComparison.Algorithms
{
    public class GreedyAlgorithm : IGraphColoring
    {
        public string Name => _sortVertices ? "Greedy with sorted vertices" : "Greedy";

        private readonly bool _sortVertices;
        private BigInteger _iterations;

        public GreedyAlgorithm(bool sortVertices = false)
        {
            _sortVertices = sortVertices;
        }

        public ColoringStats Run(Graph g)
        {
            _iterations = 0;
            Stopwatch sw = new Stopwatch();

            List<string> vertices = _sortVertices ? SortVerticesByDegree(g) : g.GetAllVertices();

            int k = 1;
            Dictionary<string, int> colors = new Dictionary<string, int>();

            sw.Start();

            colors[vertices[0]] = k - 1;

            for (int i = 1; i < vertices.Count; i++)
            {
                _iterations++;
                k = FirstAvailableColor(colors, vertices[i], g);
                colors[vertices[i]] = k;
            }

            sw.Stop();


            ColoringStats stats = new ColoringStats
            {
                ChromaticNumber = colors.Max(x => x.Value) + 1,
                Colors = colors,
                Time = sw.Elapsed,
                Iterations = _iterations
            };
            if (_sortVertices)
            {
                stats.TheoreticalComplexity = (BigInteger) (Math.Pow(g.VertCount, 2) + (int) (g.VertCount * Math.Log(g.VertCount)) + g.EdgeCount);
                stats.TheoreticalComplexityStr = "O(V*log(V) + V^2 + E)";
            }
            else
            {
                stats.TheoreticalComplexity = (BigInteger) (Math.Pow(g.VertCount, 2) + g.EdgeCount);
                stats.TheoreticalComplexityStr = "O(V^2 + E)";
            }

            _iterations = 0;

            return stats;
        }

        private int FirstAvailableColor(IReadOnlyDictionary<string, int> colors, string vertex, Graph g)
        {
            bool[] colorUsed = new bool[g.VertCount];

            foreach (string neighbor in g.GetNeighbors(vertex))
            {
                _iterations++;
                if (colors.ContainsKey(neighbor))
                    colorUsed[colors[neighbor]] = true;
            }

            for (int i = 0; i < colorUsed.Length; i++)
            {
                _iterations++;
                if (!colorUsed[i])
                    return i;
            }

            return g.VertCount;
        }

        private List<string> SortVerticesByDegree(Graph g)
        {
            List<string> vertices = g.GetAllVertices();
            vertices = vertices.OrderByDescending(g.CalculateDegree).ToList();

            //zlozonosc OrderBy to O(n log n) - uzywa quick sorta
            _iterations += (int)(g.VertCount * Math.Log(g.VertCount));

            return vertices;
        }
    }
}
