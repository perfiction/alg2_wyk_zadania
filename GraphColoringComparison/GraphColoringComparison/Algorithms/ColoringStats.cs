﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace GraphColoringComparison.Algorithms
{
    public class ColoringStats
    {
        public int ChromaticNumber { get; set; }
        public Dictionary<string, int> Colors { get; set; }
        public TimeSpan Time { get; set; }
        public BigInteger Iterations { get; set; }
        public BigInteger TheoreticalComplexity { get; set; }
        public string TheoreticalComplexityStr { get; set; }
    }
}
