﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;

namespace GraphColoringComparison.Algorithms
{
    public class GreedyBacktrackingAlgorithm : IGraphColoring
    {
        public string Name => "Sequential with backtracking";

        private BigInteger _iterations;

        public ColoringStats Run(Graph g)
        {
            Stopwatch sw = new Stopwatch();
            _iterations = 0;

            Dictionary<string, int> colors = new Dictionary<string, int>();
            List<string> vertices = g.GetAllVertices();
            int lowerBound = g.VertCount;

            sw.Start();
            BacktrackingRecursion(g, 0, vertices, colors, lowerBound);
            sw.Stop();


            ColoringStats stats = new ColoringStats
            {
                ChromaticNumber = colors.Max(x => x.Value) + 1,
                Colors = colors,
                Time = sw.Elapsed,
                Iterations = _iterations,
                TheoreticalComplexity = (BigInteger)Math.Pow(2, g.VertCount),
                TheoreticalComplexityStr = "O(2^V)"
            };

            _iterations = 0;

            return stats;
        }

        private bool BacktrackingRecursion(Graph g, int vertIndex, IReadOnlyList<string> vertices, Dictionary<string, int> colors, int lowerBound)
        {
            if (colors.Count == g.VertCount)
                return true;

            for (int c = 0; c <= lowerBound; c++)
            {
                _iterations++;
                if (!IsSafe(g, vertices[vertIndex], colors, c)) continue;

                colors[vertices[vertIndex]] = c;

                if (BacktrackingRecursion(g, vertIndex + 1, vertices, colors, lowerBound))
                    return true;

                colors.Remove(vertices[vertIndex]);
            }

            return false;
        }

        private bool IsSafe(Graph g, string vertex, IReadOnlyDictionary<string, int> colors, int color)
        {
            foreach (string neighbor in g.GetNeighbors(vertex))
            {
                _iterations++;
                if (colors.ContainsKey(neighbor) && colors[neighbor] == color)
                    return false;
            }


            return true;
        }
    }
}
