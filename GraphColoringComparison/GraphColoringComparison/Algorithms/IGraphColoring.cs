﻿namespace GraphColoringComparison.Algorithms
{
    public interface IGraphColoring
    {
        string Name { get; }
        ColoringStats Run(Graph g);
    }
}
