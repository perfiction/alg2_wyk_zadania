﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;

namespace GraphColoringComparison.Algorithms
{
    public class Christofides : IGraphColoring
    {
        public string Name => "Christofides (Maximal independent sets)";
        private BigInteger _iterations;

        public ColoringStats Run(Graph g)
        {
            _iterations = 0;
            Stopwatch sw = new Stopwatch();

            List<string> u = new List<string>(g.GetAllVertices());

            byte k = 0;
            Dictionary<string, int> colors = new Dictionary<string, int>();

            sw.Start();

            while (u.Count != 0)
            {
                _iterations++;
                k++;
                IEnumerable<string> w = MaximumIndependentSubset(g, u);

                foreach (string vertex in w)
                {
                    _iterations++;
                    colors[vertex] = k - 1;
                    u.Remove(vertex);
                }
            }

            sw.Stop();

            ColoringStats output = new ColoringStats
            {
                ChromaticNumber = k,
                Colors = colors,
                Time = sw.Elapsed,
                Iterations = _iterations,
                TheoreticalComplexity = (BigInteger)(g.VertCount * g.EdgeCount * Math.Pow(2.44, g.VertCount)),
                TheoreticalComplexityStr = "O(V * E * 2.44^V)"
            };

            _iterations = 0;

            return output;
        }

        private IEnumerable<string> MaximumIndependentSubset(Graph g, IEnumerable<string> u)
        {
            List<string> u1 = new List<string>(u);
            List<string> w = new List<string>();

            while (u1.Count != 0)
            {
                _iterations++;
                Graph subGraph = CreateSubGraph(g, u1);

                //find vertex with minimum degree in sub graph
                string vertexWithMinDegree = null;
                int minDegree = int.MaxValue;
                foreach (string vertex in u1)
                {
                    _iterations++;
                    int degree = subGraph.CalculateDegree(vertex);
                    if (degree >= minDegree) continue;

                    vertexWithMinDegree = vertex;
                    minDegree = degree;
                }

                //add this vertex to W
                w.Add(vertexWithMinDegree);

                // remove this vertex and all of his neighbors from U1
                u1.Remove(vertexWithMinDegree);
                foreach (string neighbor in g.GetNeighbors(vertexWithMinDegree))
                {
                    _iterations++;
                    u1.Remove(neighbor);
                }
            }

            return w;
        }

        private Graph CreateSubGraph(Graph g, ICollection<string> vertices)
        {
            Graph subGraph = new Graph();

            foreach ((string from, string to) in g.GetAllEdges())
            {
                _iterations++;
                if (vertices.Contains(from) && vertices.Contains(to))
                    subGraph.AddEdge(from, to);
            }

            return subGraph;
        }
    }
}
