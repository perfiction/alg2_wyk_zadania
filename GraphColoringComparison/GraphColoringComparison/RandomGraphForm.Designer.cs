﻿namespace GraphColoringComparison
{
    partial class RandomGraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.vertexCountNud = new System.Windows.Forms.NumericUpDown();
            this.edgeCountNud = new System.Windows.Forms.NumericUpDown();
            this.generateButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.vertexCountNud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgeCountNud)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vertex count:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Edge count:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // vertexCountNud
            // 
            this.vertexCountNud.Location = new System.Drawing.Point(118, 12);
            this.vertexCountNud.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.vertexCountNud.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.vertexCountNud.Name = "vertexCountNud";
            this.vertexCountNud.Size = new System.Drawing.Size(120, 20);
            this.vertexCountNud.TabIndex = 2;
            this.vertexCountNud.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.vertexCountNud.ValueChanged += new System.EventHandler(this.vertexCountNud_ValueChanged);
            // 
            // edgeCountNud
            // 
            this.edgeCountNud.Location = new System.Drawing.Point(118, 38);
            this.edgeCountNud.Name = "edgeCountNud";
            this.edgeCountNud.Size = new System.Drawing.Size(120, 20);
            this.edgeCountNud.TabIndex = 3;
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(163, 79);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(75, 23);
            this.generateButton.TabIndex = 4;
            this.generateButton.Text = "Generate";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // RandomGraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 116);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.edgeCountNud);
            this.Controls.Add(this.vertexCountNud);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RandomGraphForm";
            this.Text = "Random graph generation";
            ((System.ComponentModel.ISupportInitialize)(this.vertexCountNud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgeCountNud)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown vertexCountNud;
        private System.Windows.Forms.NumericUpDown edgeCountNud;
        private System.Windows.Forms.Button generateButton;
    }
}