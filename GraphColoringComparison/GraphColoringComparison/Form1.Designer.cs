﻿namespace GraphColoringComparison
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableGraphAntialiasingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawingGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbAlgoGreedySorted = new System.Windows.Forms.CheckBox();
            this.cbAlgoGreedyBacktrack = new System.Windows.Forms.CheckBox();
            this.cbAlgoGreedy = new System.Windows.Forms.CheckBox();
            this.cbAlgoChristo = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCompare = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.graphView = new GraphColoringComparison.Visualization.GraphView();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.generateRandomGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(944, 24);
            this.menuStrip.TabIndex = 3;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateRandomGraphToolStripMenuItem,
            this.clearGraphToolStripMenuItem,
            this.loadGraphToolStripMenuItem,
            this.saveGraphToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.fileToolStripMenuItem.Text = "Graph";
            // 
            // clearGraphToolStripMenuItem
            // 
            this.clearGraphToolStripMenuItem.Name = "clearGraphToolStripMenuItem";
            this.clearGraphToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.clearGraphToolStripMenuItem.Text = "Clear graph";
            this.clearGraphToolStripMenuItem.Click += new System.EventHandler(this.clearGraphToolStripMenuItem_Click);
            // 
            // loadGraphToolStripMenuItem
            // 
            this.loadGraphToolStripMenuItem.Name = "loadGraphToolStripMenuItem";
            this.loadGraphToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadGraphToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.loadGraphToolStripMenuItem.Text = "Open graph from file...";
            this.loadGraphToolStripMenuItem.Click += new System.EventHandler(this.loadGraphToolStripMenuItem_Click);
            // 
            // saveGraphToolStripMenuItem
            // 
            this.saveGraphToolStripMenuItem.Name = "saveGraphToolStripMenuItem";
            this.saveGraphToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveGraphToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.saveGraphToolStripMenuItem.Text = "Save graph to file...";
            this.saveGraphToolStripMenuItem.Click += new System.EventHandler(this.saveGraphToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enableGraphAntialiasingToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // enableGraphAntialiasingToolStripMenuItem
            // 
            this.enableGraphAntialiasingToolStripMenuItem.Checked = true;
            this.enableGraphAntialiasingToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableGraphAntialiasingToolStripMenuItem.Name = "enableGraphAntialiasingToolStripMenuItem";
            this.enableGraphAntialiasingToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.enableGraphAntialiasingToolStripMenuItem.Text = "Enable graph anti-aliasing";
            this.enableGraphAntialiasingToolStripMenuItem.Click += new System.EventHandler(this.enableGraphAntialiasingToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.drawingGraphToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // drawingGraphToolStripMenuItem
            // 
            this.drawingGraphToolStripMenuItem.Name = "drawingGraphToolStripMenuItem";
            this.drawingGraphToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.drawingGraphToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.drawingGraphToolStripMenuItem.Text = "How to draw a graph?";
            this.drawingGraphToolStripMenuItem.Click += new System.EventHandler(this.drawingGraphToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.aboutToolStripMenuItem.Text = "About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbAlgoGreedySorted);
            this.groupBox1.Controls.Add(this.cbAlgoGreedyBacktrack);
            this.groupBox1.Controls.Add(this.cbAlgoGreedy);
            this.groupBox1.Controls.Add(this.cbAlgoChristo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnCompare);
            this.groupBox1.Location = new System.Drawing.Point(705, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(227, 462);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Comparison settings";
            // 
            // cbAlgoGreedySorted
            // 
            this.cbAlgoGreedySorted.AutoSize = true;
            this.cbAlgoGreedySorted.Checked = true;
            this.cbAlgoGreedySorted.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAlgoGreedySorted.Location = new System.Drawing.Point(9, 102);
            this.cbAlgoGreedySorted.Name = "cbAlgoGreedySorted";
            this.cbAlgoGreedySorted.Size = new System.Drawing.Size(154, 17);
            this.cbAlgoGreedySorted.TabIndex = 6;
            this.cbAlgoGreedySorted.Text = "Greedy with sorted vertices";
            this.cbAlgoGreedySorted.UseVisualStyleBackColor = true;
            // 
            // cbAlgoGreedyBacktrack
            // 
            this.cbAlgoGreedyBacktrack.AutoSize = true;
            this.cbAlgoGreedyBacktrack.Checked = true;
            this.cbAlgoGreedyBacktrack.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAlgoGreedyBacktrack.Location = new System.Drawing.Point(9, 125);
            this.cbAlgoGreedyBacktrack.Name = "cbAlgoGreedyBacktrack";
            this.cbAlgoGreedyBacktrack.Size = new System.Drawing.Size(163, 17);
            this.cbAlgoGreedyBacktrack.TabIndex = 5;
            this.cbAlgoGreedyBacktrack.Text = "Sequential with backtracking";
            this.cbAlgoGreedyBacktrack.UseVisualStyleBackColor = true;
            // 
            // cbAlgoGreedy
            // 
            this.cbAlgoGreedy.AutoSize = true;
            this.cbAlgoGreedy.Checked = true;
            this.cbAlgoGreedy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAlgoGreedy.Location = new System.Drawing.Point(9, 79);
            this.cbAlgoGreedy.Name = "cbAlgoGreedy";
            this.cbAlgoGreedy.Size = new System.Drawing.Size(119, 17);
            this.cbAlgoGreedy.TabIndex = 4;
            this.cbAlgoGreedy.Text = "Greedy (Sequential)";
            this.cbAlgoGreedy.UseVisualStyleBackColor = true;
            // 
            // cbAlgoChristo
            // 
            this.cbAlgoChristo.AutoSize = true;
            this.cbAlgoChristo.Checked = true;
            this.cbAlgoChristo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAlgoChristo.Location = new System.Drawing.Point(9, 56);
            this.cbAlgoChristo.Name = "cbAlgoChristo";
            this.cbAlgoChristo.Size = new System.Drawing.Size(210, 17);
            this.cbAlgoChristo.TabIndex = 2;
            this.cbAlgoChristo.Text = "Christofides (Max independent subsets)";
            this.cbAlgoChristo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Algorithms to compare:";
            // 
            // btnCompare
            // 
            this.btnCompare.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCompare.Location = new System.Drawing.Point(6, 424);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(215, 32);
            this.btnCompare.TabIndex = 0;
            this.btnCompare.Text = "Compare algorithms";
            this.btnCompare.UseVisualStyleBackColor = true;
            this.btnCompare.Click += new System.EventHandler(this.btnCompare_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.graphView);
            this.groupBox2.Location = new System.Drawing.Point(12, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(690, 462);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Graph view";
            // 
            // graphView
            // 
            this.graphView.Antialiasing = true;
            this.graphView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphView.Location = new System.Drawing.Point(3, 16);
            this.graphView.Name = "graphView";
            this.graphView.PresentationMode = false;
            this.graphView.Size = new System.Drawing.Size(684, 443);
            this.graphView.TabIndex = 4;
            this.graphView.VertexLocations = ((System.Collections.Generic.Dictionary<string, System.Drawing.PointF>)(resources.GetObject("graphView.VertexLocations")));
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "graph";
            this.openFileDialog.Filter = "Graph files|*.graph|All files|*.*";
            this.openFileDialog.Title = "Open graph file";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Graph files|*.graph|All files|*.*";
            this.saveFileDialog.Title = "Save graph file";
            // 
            // generateRandomGraphToolStripMenuItem
            // 
            this.generateRandomGraphToolStripMenuItem.Name = "generateRandomGraphToolStripMenuItem";
            this.generateRandomGraphToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.generateRandomGraphToolStripMenuItem.Text = "Generate random graph...";
            this.generateRandomGraphToolStripMenuItem.Click += new System.EventHandler(this.generateRandomGraphToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip);
            this.MinimumSize = new System.Drawing.Size(768, 432);
            this.Name = "Form1";
            this.Text = "Graph coloring comparison";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drawingGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private GraphColoringComparison.Visualization.GraphView graphView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbAlgoGreedyBacktrack;
        private System.Windows.Forms.CheckBox cbAlgoGreedy;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.ToolStripMenuItem clearGraphToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableGraphAntialiasingToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbAlgoGreedySorted;
        private System.Windows.Forms.CheckBox cbAlgoChristo;
        private System.Windows.Forms.ToolStripMenuItem generateRandomGraphToolStripMenuItem;
    }
}

