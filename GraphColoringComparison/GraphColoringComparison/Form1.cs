﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GraphColoringComparison.Algorithms;
using GraphColoringComparison.OutputPresentation;

namespace GraphColoringComparison
{
    public partial class Form1 : Form
    {
        private Graph _g;
        public Form1()
        {
            InitializeComponent();
            _g = new Graph();
            graphView.SetGraph(_g);
        }

        private void drawingGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            const string message = "Click Left Mouse Button on the empty area to create new vertex.\n" +
                                   "Click Left Mouse Button on existing vertex, then click again on other vertex to add edge between them.\n" +
                                   "Click Right mouse button during creating new edge to cancel it.\n" +
                                   "Click Right mouse button on existing vertex or edge to remove it.\n";
            MessageBox.Show(message, "How to draw a graph", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            const string message = "Application made by Leszek Bonikowski\n\n" +
                                   "Implemented coloring algorithms are based on\nhttp://fizyka.umk.pl/~raad/optymalizacja.pdf";
            MessageBox.Show(message, "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnCompare_Click(object sender, EventArgs e)
        {
            if (_g.VertCount == 0)
            {
                MessageBox.Show("Create graph or load it from file first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            List<IGraphColoring> methodsToTest = new List<IGraphColoring>();

            if (cbAlgoChristo.Checked) methodsToTest.Add(new Christofides());
            if (cbAlgoGreedy.Checked) methodsToTest.Add(new GreedyAlgorithm());
            if (cbAlgoGreedySorted.Checked) methodsToTest.Add(new GreedyAlgorithm(true));
            if (cbAlgoGreedyBacktrack.Checked) methodsToTest.Add(new GreedyBacktrackingAlgorithm());

            if (methodsToTest.Count == 0)
            {
                MessageBox.Show("You must select at least one algorithm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            btnCompare.Enabled = false;
            graphView.Enabled = false;

            backgroundWorker.RunWorkerAsync(methodsToTest);
        }

        private void backgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            List<IGraphColoring> methodsToTest = (List<IGraphColoring>)e.Argument;
            List<ColoringStats> outputs = new List<ColoringStats>();

            foreach (IGraphColoring method in methodsToTest)
            {
                var stats = method.Run(_g);
                outputs.Add(stats);
                e.Result = (methodsToTest, outputs);
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            btnCompare.Enabled = true;
            graphView.Enabled = true;

            (List<IGraphColoring> methods, List<ColoringStats> outputs) = (ValueTuple<List<IGraphColoring>, List<ColoringStats>>)e.Result;

            PresentationForm presentation = new PresentationForm(_g, graphView.VertexLocations, methods, outputs, graphView.Antialiasing);
            presentation.StartPosition = FormStartPosition.CenterParent;
            presentation.ShowDialog(this);
        }

        private void clearGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (string vertex in _g.GetAllVertices())
                _g.RemoveVerted(vertex);
            graphView.VertexLocations.Clear();
            graphView.Invalidate();
        }

        private void loadGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();
            if (result != DialogResult.OK) return;

            (_g, graphView.VertexLocations) = FileHelper.LoadGraphFromFile(openFileDialog.FileName);
            graphView.SetGraph(_g);
            graphView.Invalidate();
        }

        private void saveGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog.ShowDialog();
            if (result != DialogResult.OK) return;

            FileHelper.SaveGraphToFile(saveFileDialog.FileName, _g, graphView.VertexLocations);
        }

        private void enableGraphAntialiasingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsSender = (ToolStripMenuItem)sender;
            tsSender.Checked = !tsSender.Checked;
            graphView.Antialiasing = tsSender.Checked;
        }

        private void generateRandomGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RandomGraphForm randomGraphForm = new RandomGraphForm();
            DialogResult result = randomGraphForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                _g = randomGraphForm.Output;
                graphView.SetGraph(_g);
                graphView.VertexLocations = randomGraphForm.Locations;
                graphView.Invalidate();
            }
        }
    }
}
