﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;

namespace GraphColoringComparison
{
    public static class FileHelper
    {
        public static void SaveGraphToFile(string filePath, Graph g, Dictionary<string, PointF> vertexLocations)
        {
            StreamWriter writer = File.CreateText(filePath);

            writer.WriteLine("#VERTICES " + g.VertCount);
            foreach (string vertex in g.GetAllVertices())
                writer.WriteLine("{0};{1};{2}", vertex, vertexLocations[vertex].X.ToString(CultureInfo.InvariantCulture), vertexLocations[vertex].Y.ToString(CultureInfo.InvariantCulture));

            writer.WriteLine("#EDGES");
            foreach ((string from, string to) in g.GetAllEdges())
                writer.WriteLine("{0};{1}", from, to);

            writer.Close();
        }

        public static (Graph, Dictionary<string, PointF>) LoadGraphFromFile(string filePath)
        {
            Graph g = new Graph();
            Dictionary<string, PointF> vertexLocations = new Dictionary<string, PointF>();

            StreamReader reader = new StreamReader(filePath);
            string line = reader.ReadLine();

            if (!line.StartsWith("#VERTICES"))
                throw new IOException("Wrong input file format - missing #VERTICES keyword");

            int vertexCount = int.Parse(line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1]);

            for (int i = 0; i < vertexCount; i++)
            {
                line = reader.ReadLine();
                string[] split = line.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                g.AddVertex(split[0]);
                vertexLocations.Add(split[0], new PointF(float.Parse(split[1], CultureInfo.InvariantCulture), float.Parse(split[2], CultureInfo.InvariantCulture)));
            }

            line = reader.ReadLine();
            if (!line.StartsWith("#EDGES"))
                throw new IOException("Wrong input file format - missing #EDGES keyword");

            while ((line = reader.ReadLine()) != null)
            {
                string[] split = line.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                g.AddEdge(split[0], split[1]);
            }

            reader.Close();

            return (g, vertexLocations);
        }
    }
}
