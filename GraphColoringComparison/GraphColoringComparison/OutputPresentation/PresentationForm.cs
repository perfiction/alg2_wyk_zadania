﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using GraphColoringComparison.Algorithms;
using GraphColoringComparison.Visualization;

namespace GraphColoringComparison.OutputPresentation
{
    public partial class PresentationForm : Form
    {
        public PresentationForm()
        {
            InitializeComponent();
        }

        public PresentationForm(Graph g, Dictionary<string, PointF> vertexLocations, IReadOnlyList<IGraphColoring> methods, IReadOnlyList<ColoringStats> outputs, bool antiAlias)
        {
            InitializeComponent();

            for (int i = 0; i < methods.Count; i++)
            {
                dataGridView.Rows.Add(methods[i].Name, outputs[i].ChromaticNumber, outputs[i].Time.TotalMilliseconds, outputs[i].Iterations.ToString("e"), outputs[i].TheoreticalComplexity.ToString("e"), outputs[i].TheoreticalComplexityStr);

                //Nowa zakladka z wizualizacja:
                tabControl.TabPages.Add(GenerateTabPage(methods[i].Name, g, vertexLocations, outputs[i].Colors, antiAlias));
            }
        }

        private static TabPage GenerateTabPage(string name, Graph g, Dictionary<string, PointF> vertexLocations, Dictionary<string, int> colors, bool antiAlias)
        {
            TabPage tabPage = new TabPage(name);

            GraphView graphView = new GraphView();
            graphView.SetGraph(g);
            graphView.VertexLocations = vertexLocations;
            graphView.SetColors(colors);
            graphView.Antialiasing = antiAlias;
            graphView.Parent = tabPage;
            graphView.Dock = DockStyle.Fill;
            graphView.PresentationMode = true;

            tabPage.Controls.Add(graphView);

            return tabPage;
        }
    }
}
