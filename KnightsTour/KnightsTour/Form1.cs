﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using KnightsTour.Algorithms;
using KnightsTour.DataStructures;

namespace KnightsTour
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            warnsdorffTypeCbx.SelectedIndex = 0;
        }

        private void boardSizeNuds_ValueChanged(object sender, EventArgs e)
        {
            if (boardWidthNud.Value != boardHeightNud.Value && boardWidthNud.Value != boardHeightNud.Value + 2)
            {
                errorProvider.SetError(boardWidthNud, "Width must be equal to height or height + 2");
                errorProvider.SetError(boardHeightNud, "Width must be equal to height or height + 2");
                startBtn.Enabled = false;
            }
            else if (boardHeightNud.Value % 2 != 0)
            {
                errorProvider.SetError(boardWidthNud, string.Empty);
                errorProvider.SetError(boardHeightNud, "Height must be an even number");
            }
            else
            {
                errorProvider.Clear();
                startBtn.Enabled = true;
            }
        }

        private void animateVisCb_CheckedChanged(object sender, EventArgs e)
        {
            visDelayNud.Enabled = animateVisCb.Checked;
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            int width = (int)boardWidthNud.Value;
            int height = (int)boardHeightNud.Value;

            Warnsdorff.Type type = warnsdorffTypeCbx.SelectedIndex == 0 ? Warnsdorff.Type.Recursive : Warnsdorff.Type.Iterative;

            Parberry parberry = new Parberry(type, width, height);
            Tour tour = parberry.CalculateTour();
            List<Point> list = tour.ToList();
            int iterations = parberry.TotalIterations;

            new VisualizationForm(animateVisCb.Checked ? (int)visDelayNud.Value : 0, (width, height), list, iterations).Show();
        }
    }
}
