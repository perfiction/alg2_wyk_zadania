﻿namespace KnightsTour
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.boardWidthNud = new System.Windows.Forms.NumericUpDown();
            this.boardHeightNud = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.visDelayNud = new System.Windows.Forms.NumericUpDown();
            this.animateVisCb = new System.Windows.Forms.CheckBox();
            this.startBtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.warnsdorffTypeCbx = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.boardWidthNud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardHeightNud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visDelayNud)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // boardWidthNud
            // 
            this.boardWidthNud.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.boardWidthNud.Location = new System.Drawing.Point(128, 16);
            this.boardWidthNud.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.boardWidthNud.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.boardWidthNud.Name = "boardWidthNud";
            this.boardWidthNud.Size = new System.Drawing.Size(160, 20);
            this.boardWidthNud.TabIndex = 0;
            this.boardWidthNud.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.boardWidthNud.ValueChanged += new System.EventHandler(this.boardSizeNuds_ValueChanged);
            // 
            // boardHeightNud
            // 
            this.boardHeightNud.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.boardHeightNud.Location = new System.Drawing.Point(128, 42);
            this.boardHeightNud.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.boardHeightNud.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.boardHeightNud.Name = "boardHeightNud";
            this.boardHeightNud.Size = new System.Drawing.Size(160, 20);
            this.boardHeightNud.TabIndex = 1;
            this.boardHeightNud.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.boardHeightNud.ValueChanged += new System.EventHandler(this.boardSizeNuds_ValueChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Board width:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Board height:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.boardWidthNud);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.boardHeightNud);
            this.groupBox1.Location = new System.Drawing.Point(12, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 68);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Board settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.visDelayNud);
            this.groupBox2.Controls.Add(this.animateVisCb);
            this.groupBox2.Location = new System.Drawing.Point(12, 145);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(310, 65);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Visualization settings";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Animation delay [ms]:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // visDelayNud
            // 
            this.visDelayNud.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.visDelayNud.Location = new System.Drawing.Point(128, 39);
            this.visDelayNud.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.visDelayNud.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.visDelayNud.Name = "visDelayNud";
            this.visDelayNud.Size = new System.Drawing.Size(176, 20);
            this.visDelayNud.TabIndex = 1;
            this.visDelayNud.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // animateVisCb
            // 
            this.animateVisCb.AutoSize = true;
            this.animateVisCb.Checked = true;
            this.animateVisCb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.animateVisCb.Location = new System.Drawing.Point(6, 19);
            this.animateVisCb.Name = "animateVisCb";
            this.animateVisCb.Size = new System.Drawing.Size(133, 17);
            this.animateVisCb.TabIndex = 0;
            this.animateVisCb.Text = "Step by step animation";
            this.animateVisCb.UseVisualStyleBackColor = true;
            this.animateVisCb.CheckedChanged += new System.EventHandler(this.animateVisCb_CheckedChanged);
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(247, 216);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(75, 23);
            this.startBtn.TabIndex = 6;
            this.startBtn.Text = "Start";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.warnsdorffTypeCbx);
            this.groupBox3.Location = new System.Drawing.Point(12, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 46);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Algorithm settings";
            // 
            // warnsdorffTypeCbx
            // 
            this.warnsdorffTypeCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.warnsdorffTypeCbx.FormattingEnabled = true;
            this.warnsdorffTypeCbx.Items.AddRange(new object[] {
            "Recursive",
            "Iterative"});
            this.warnsdorffTypeCbx.Location = new System.Drawing.Point(128, 19);
            this.warnsdorffTypeCbx.Name = "warnsdorffTypeCbx";
            this.warnsdorffTypeCbx.Size = new System.Drawing.Size(176, 21);
            this.warnsdorffTypeCbx.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "Warnsdorff\'s algorithm:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 251);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Closed knight\'s tour";
            ((System.ComponentModel.ISupportInitialize)(this.boardWidthNud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardHeightNud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visDelayNud)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown boardWidthNud;
        private System.Windows.Forms.NumericUpDown boardHeightNud;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown visDelayNud;
        private System.Windows.Forms.CheckBox animateVisCb;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox warnsdorffTypeCbx;
    }
}

