﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using KnightsTour.DataStructures;

namespace KnightsTour.Algorithms
{
    public class WarnsdorffRecursive : Warnsdorff
    {
        public int Attempts { get; set; }

        public WarnsdorffRecursive(int width, int height) : base(width, height) { }

        public override Tour CalculateClosedTour(Point offset)
        {
            Attempts = 0;
            Tour tour = null;
            bool foundSolution = false;

            while (!foundSolution)
            {
                Attempts++;

                tour = new Tour();
                int[,] board = new int[Height, Width];
                int[,] freeNeighbors = InitializeFreeNeighborsArray();

                Point startPoint = new Point(Rand.Next(Width), Rand.Next(Height));
                board[startPoint.Y, startPoint.X] = 1;
                tour.List.Add(startPoint);
                FixFreeNeighborsArray(startPoint, freeNeighbors, -1);
                bool q = false;

                CalculateTour(1, board, freeNeighbors, tour, startPoint, ref q);

                if (tour.List.Count == Width * Height && IsClosed(startPoint, tour.List.Tail.Value))
                {
                    var moves = GetMoves(tour);
                    if (IsStructured(moves))
                        foundSolution = true;
                }
            }

            // Add offsets to completed tour and update structured points
            Node<Point> node = tour.List.Head;
            while (node != null)
            {
                int val = UpdateStructuredPosition(node.Value);
                if (val != -1)
                    tour.Structured[val] = node;

                node.Value = new Point(node.Value.X + offset.X, node.Value.Y + offset.Y);
                node = node.Next;
            }

            // Make the list cyclic
            tour.List.Head.Prev = tour.List.Tail;
            tour.List.Tail.Next = tour.List.Head;

            // Set start node
            tour.List.StartNode = tour.List.Head;

            return tour;
        }

        public void CalculateTour(int i, int[,] board, int[,] freeNeighbors, Tour tour, Point currentPoint, ref bool q)
        {
            Point p = SelectNextMove(currentPoint, board, freeNeighbors);
            if (p == InvalidPoint)
            {
                q = true;
                return;
            }

            board[p.Y, p.X] = i;
            FixFreeNeighborsArray(p, freeNeighbors, -1);
            tour.List.Add(p);

            if (i < Width * Height)
            {
                Iterations++;

                CalculateTour(i + 1, board, freeNeighbors, tour, p, ref q);
                if (!q)
                {
                    board[p.Y, p.X] = 0;
                    tour.List.Remove(p);

                    FixFreeNeighborsArray(p, freeNeighbors, 1);
                }
            }
            else
                q = true;
        }

        private Point SelectNextMove(Point currentPoint, int[,] board, int[,] freeNeighbors)
        {
            //Collect allowed moves and their weights
            Dictionary<int, int> moves = new Dictionary<int, int>();
            for (int k = 0; k < 8; k++)
            {
                Point nextPoint = new Point(currentPoint.X + MoveX[k], currentPoint.Y + MoveY[k]);

                if (!InsideBoard(nextPoint) || board[nextPoint.Y, nextPoint.X] != 0)
                    continue;

                moves.Add(k, freeNeighbors[nextPoint.Y, nextPoint.X]);
            }

            if (moves.Count == 0)
                return InvalidPoint;

            //Select move with lowest weight
            var minimalMoves = moves.Where(e => e.Value == moves.Min(e2 => e2.Value)).ToDictionary(e => e.Key, e => e.Value);
            int randomIndex = minimalMoves.Keys.ToList()[Rand.Next(minimalMoves.Count)];

            return new Point(currentPoint.X + MoveX[randomIndex], currentPoint.Y + MoveY[randomIndex]);
        }

        public int[,] InitializeFreeNeighborsArray()
        {
            int[,] output = new int[Height, Width];

            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    output[i, j] = 8;
                }
            }

            //2
            output[0, 0] = output[0, Width - 1] = output[Height - 1, 0] = output[Height - 1, Width - 1] = 2;

            //3
            output[0, 1] = output[1, 0] = 3;
            output[0, Width - 2] = output[1, Width - 1] = 3;
            output[Height - 2, 0] = output[Height - 1, 1] = 3;
            output[Height - 2, Width - 1] = output[Height - 1, Width - 2] = 3;

            //4
            for (int i = 2; i < Height - 2; i++)
            {
                output[i, 0] = 4;
                output[i, Width - 1] = 4;
            }

            for (int j = 2; j < Width - 2; j++)
            {
                output[0, j] = 4;
                output[Height - 1, j] = 4;
            }
            output[1, 1] = output[1, Width - 2] = output[Height - 2, Width - 2] = output[Height - 2, 1] = 4;

            //6
            for (int i = 2; i < Height - 2; i++)
            {
                output[i, 1] = 6;
                output[i, Width - 2] = 6;
            }

            for (int j = 2; j < Width - 2; j++)
            {
                output[1, j] = 6;
                output[Height - 2, j] = 6;
            }

            return output;
        }

        private void FixFreeNeighborsArray(Point p, int[,] freeNeighbors, int val)
        {
            for (int i = 0; i < 8; i++)
            {
                Point nextPoint = new Point(p.X + MoveX[i], p.Y + MoveY[i]);

                if (InsideBoard(nextPoint))
                    freeNeighbors[nextPoint.Y, nextPoint.X] += val;
            }
        }
    }
}
