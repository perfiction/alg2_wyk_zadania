﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using KnightsTour.DataStructures;

namespace KnightsTour.Algorithms
{
    public class WarnsdorffIterative : Warnsdorff
    {
        public int Attempts { get; set; }

        public WarnsdorffIterative(int width, int height) : base(width, height) { }

        public override Tour CalculateClosedTour(Point offset)
        {
            Attempts = 0;
            Tour tour = null;
            bool foundSolution = false;

            // Main loop
            while (!foundSolution)
            {
                Attempts++;

                Point startPoint = new Point(Rand.Next(Width), Rand.Next(Height));
                tour = CalculateTour(startPoint);
                if (tour.List.Count == Width * Height && IsClosed(startPoint, tour.List.Tail.Value))
                {
                    var moves = GetMoves(tour);
                    if (IsStructured(moves))
                        foundSolution = true;
                }
            }

            // Add offsets to completed tour
            Node<Point> node = tour.List.Head;
            while (node != null)
            {
                node.Value = new Point(node.Value.X + offset.X, node.Value.Y + offset.Y);
                node = node.Next;
            }

            // Make the list cyclic
            tour.List.Head.Prev = tour.List.Tail;
            tour.List.Tail.Next = tour.List.Head;

            // Set start node
            tour.List.StartNode = tour.List.Head;

            return tour;
        }

        private Tour CalculateTour(Point startPoint)
        {
            Tour tour = new Tour();
            int i = 0;

            bool[,] visited = new bool[Height, Width];

            Point currentPoint = startPoint;

            while (i < Width * Height)
            {
                Iterations++;

                visited[currentPoint.Y, currentPoint.X] = true;
                tour.List.Add(currentPoint);

                int val = UpdateStructuredPosition(currentPoint);
                if (val != -1)
                    tour.Structured[val] = tour.List.Tail;

                i++;
                currentPoint = SelectNextMove(currentPoint, visited);
                if (currentPoint == InvalidPoint)
                    break;
            }

            return tour;
        }

        private Point SelectNextMove(Point currentPoint, bool[,] visited)
        {
            //Collect allowed moves and their weights
            Dictionary<Point, int> moves = new Dictionary<Point, int>();
            for (int k = 0; k < 8; k++)
            {
                Point nextPoint = new Point(currentPoint.X + MoveX[k], currentPoint.Y + MoveY[k]);
                if (!InsideBoard(nextPoint) || visited[nextPoint.Y, nextPoint.X])
                    continue;

                moves.Add(nextPoint, GetAvailableMovesCount(visited, nextPoint));
            }

            if (moves.Count == 0)
                return InvalidPoint;

            //Select move with lowest weight
            var minimalMoves = moves.Where(e => e.Value == moves.Min(e2 => e2.Value)).ToDictionary(e => e.Key, e => e.Value);

            return minimalMoves.Count > 1 ? HandleTie(minimalMoves) : minimalMoves.First().Key;
        }

        private Point HandleTie(Dictionary<Point, int> points)
        {
            Point center = new Point(Width / 2, Height / 2);
            Point output = InvalidPoint;
            double maxDistance = double.MinValue;

            foreach (Point p in points.Keys)
            {
                double currentDistance = Distance(center, p);
                if (currentDistance > maxDistance)
                {
                    maxDistance = currentDistance;
                    output = p;
                }
            }

            return output;
        }

        private static double Distance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        private int GetAvailableMovesCount(bool[,] visited, Point p)
        {
            int count = 0;

            for (int i = 0; i < 8; i++)
            {
                Point newP = new Point(p.X + MoveX[i], p.Y + MoveY[i]);
                if (InsideBoard(newP) && !visited[newP.Y, newP.X])
                    count++;
            }

            return count;
        }
    }
}
