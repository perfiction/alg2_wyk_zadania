﻿using System;
using System.Collections.Generic;
using System.Drawing;
using KnightsTour.DataStructures;

namespace KnightsTour.Algorithms
{
    public abstract class Warnsdorff
    {
        public enum Type { Recursive, Iterative }

        protected int Width { get; set; }
        protected int Height { get; set; }
        protected Random Rand { get; set; }

        public int Iterations { get; set; }

        protected static readonly int[] MoveX = { 1, 2, 2, 1, -1, -2, -2, -1 };
        protected static readonly int[] MoveY = { 2, 1, -1, -2, -2, -1, 1, 2 };
        protected static readonly Point InvalidPoint = new Point(-1, -1);

        protected Warnsdorff(int width, int height)
        {
            Width = width;
            Height = height;
            Rand = new Random();
        }

        protected bool InsideBoard(Point p)
        {
            return p.X >= 0 && p.X < Width && p.Y >= 0 && p.Y < Height;
        }

        protected bool IsClosed(Point start, Point end)
        {
            return (Math.Abs(start.X - end.X) == 1 && Math.Abs(start.Y - end.Y) == 2) ||
                   (Math.Abs(start.X - end.X) == 2 && Math.Abs(start.Y - end.Y) == 1);
        }

        public abstract Tour CalculateClosedTour(Point offset);

        protected HashSet<(Point from, Point to)> GetMoves(Tour tour)
        {
            HashSet<(Point, Point)> moves = new HashSet<(Point, Point)>();
            var currentNode = tour.List.Head;
            var nextNode = currentNode.Next;
            while (nextNode != null)
            {
                moves.Add((currentNode.Value, nextNode.Value));
                currentNode = currentNode.Next;
                nextNode = nextNode.Next;
            }

            return moves;
        }

        protected int UpdateStructuredPosition(Point p)
        {
            int[] xPos = { 2, 0, Width - 1, Width - 2, 1, 0, Width - 1, Width - 3 };
            int[] yPos = { 0, 1, 0, 2, Height - 3, Height - 1, Height - 2, Height - 1 };

            for (int i = 0; i < 8; i++)
                if (p.X == xPos[i] && p.Y == yPos[i])
                    return i;

            return -1;
        }

        protected bool IsStructured(HashSet<(Point, Point)> moves)
        {
            Point p11 = new Point(1, 0), p12 = new Point(0, 2), p13 = new Point(2, 0), p14 = new Point(0, 1);
            bool firstConnection = (moves.Contains((p11, p12)) || moves.Contains((p12, p11))) && (moves.Contains((p13, p14)) || moves.Contains((p14, p13)));
            if (!firstConnection) return false;

            Point p21 = new Point(Width - 3, 0), p22 = new Point(Width - 1, 1), p23 = new Point(Width - 2, 0), p24 = new Point(Width - 1, 2);
            bool secondConnection = (moves.Contains((p21, p22)) || moves.Contains((p22, p21))) && (moves.Contains((p23, p24)) || moves.Contains((p24, p23)));
            if (!secondConnection) return false;

            Point p31 = new Point(0, Height - 3), p32 = new Point(1, Height - 1), p33 = new Point(0, Height - 2), p34 = new Point(2, Height - 1);
            bool thirdConnection = (moves.Contains((p31, p32)) || moves.Contains((p32, p31))) && (moves.Contains((p33, p34)) || moves.Contains((p34, p33)));
            if (!thirdConnection) return false;

            Point p41 = new Point(Width - 3, Height - 1), p42 = new Point(Width - 1, Height - 2), p43 = new Point(Width - 2, Height - 1), p44 = new Point(Width - 1, Height - 3);
            bool fourthConnection = (moves.Contains((p41, p42)) || moves.Contains((p42, p41))) && (moves.Contains((p43, p44)) || moves.Contains((p44, p43)));
            if (!fourthConnection) return false;

            return true;
        }
    }
}
