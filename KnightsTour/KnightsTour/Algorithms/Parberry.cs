﻿using System;
using System.Collections.Generic;
using System.Drawing;
using KnightsTour.DataStructures;

namespace KnightsTour.Algorithms
{
    public class Parberry
    {
        private readonly int _width, _height;
        private readonly Warnsdorff.Type _type;

        public int TotalIterations { get; set; }

        public Parberry(Warnsdorff.Type type, int width, int height)
        {
            if (height < 6 || height % 2 != 0)
                throw new ArgumentException("First dimension (" + height + ") must be even number >= 6");
            if (width != height && width != height + 2)
                throw new ArgumentException("Second dimension (" + width + ") must be equal to " + height + " or " + (height + 2));

            /*
            * Allen Schwenk, 1991 Which Rectangular Chessboards Have a Knight's Tour?.
            * 
            * Theorem: An n x m chessboard with height <= width has a closed knight's tour unless one or more of
            * these three condition holds: (a) height and width are both odd; (b) height = 1, 2, 4; (c) height = 3 and width =
            * 4, 6, 8.
            */
            int newN = Math.Min(height, width);
            int newM = Math.Max(height, width);
            if ((newN % 2 == 1 && newM % 2 == 1) || (newN == 1 || newN == 2 || newN == 4) || (newN == 3 && (newM == 4 || newM == 6 || newM == 8)))
                throw new ArgumentException("Closed knight's tour doesn't exist on " + height + "x" + width + " board");

            _width = width;
            _height = height;
            _type = type;
        }

        public Tour CalculateTour()
        {
            return CalculateTour(new Point(0, 0), new Point(_width - 1, _height - 1));
        }

        private Tour CalculateTour(Point startPoint, Point endPoint)
        {
            int width = endPoint.X - startPoint.X + 1;
            int height = endPoint.Y - startPoint.Y + 1;

            //If quadrant is small enough, solve it using Warnsdorff rule
            if (height < 12 || width < 12)
            {
                Warnsdorff warnsdorff;
                switch (_type)
                {
                    case Warnsdorff.Type.Recursive: warnsdorff = new WarnsdorffRecursive(width, height); break;
                    case Warnsdorff.Type.Iterative: warnsdorff = new WarnsdorffIterative(width, height); break;
                    default: throw new ArgumentException("Wrong Warnsdorff algorithm type");
                }

                Tour warnsdorffTour = warnsdorff.CalculateClosedTour(startPoint);
                TotalIterations += warnsdorff.Iterations;

                return warnsdorffTour;
            }

            //Divide board to 4 smaller quadrants
            GetQuadrantCoordinates(width, height, startPoint, endPoint, out var quadStarts, out var quadEnds);

            //Recursively solve quadrants
            List<Tour> subTours = new List<Tour>(4);

            for (int i = 0; i < 4; i++)
                subTours.Add(CalculateTour(quadStarts[i], quadEnds[i]));

            //Combine quadrants into full solution (254)
            return CombineQuadrants(ref subTours);
        }

        private static void GetQuadrantCoordinates(int width, int height, Point startPoint, Point endPoint, out Point[] quadStarts, out Point[] quadEnds)
        {
            quadStarts = new Point[4];
            quadEnds = new Point[4];

            int k = height / 4;
            int l = width % 4;

            if (height + 2 == width && l == 2)
            {
                quadStarts[0] = startPoint;
                quadEnds[0] = new Point(startPoint.X + 2 * k - 1, startPoint.Y + height / 2 - 1);
            }
            else
            {
                quadStarts[0] = startPoint;
                quadEnds[0] = new Point(startPoint.X + 2 * k - 1, startPoint.Y + 2 * k - 1);
            }

            quadStarts[1] = new Point(quadEnds[0].X + 1, quadStarts[0].Y);
            quadEnds[1] = new Point(endPoint.X, quadEnds[0].Y);

            quadStarts[2] = new Point(startPoint.X, quadEnds[0].Y + 1);
            quadEnds[2] = new Point(quadEnds[0].X, endPoint.Y);

            quadStarts[3] = new Point(quadEnds[0].X + 1, quadEnds[0].Y + 1);
            quadEnds[3] = endPoint;
        }

        private static Tour CombineQuadrants(ref List<Tour> subTours)
        {
            //Modify structured edges to connect quadrants
            if (subTours[0].Structured[7].Next.Equals(subTours[0].Structured[6]))
            {
                subTours[0].Structured[6].Prev = subTours[1].Structured[4];
                subTours[0].Structured[7].Next = subTours[2].Structured[2];
            }
            else
            {
                subTours[0].Structured[7].Prev = subTours[2].Structured[2];
                subTours[0].Structured[6].Next = subTours[1].Structured[4];
            }

            if (subTours[2].Structured[2].Prev.Equals(subTours[2].Structured[3]))
            {
                subTours[2].Structured[2].Prev = subTours[0].Structured[7];
                subTours[2].Structured[3].Next = subTours[3].Structured[1];
            }
            else
            {
                subTours[2].Structured[2].Next = subTours[0].Structured[7];
                subTours[2].Structured[3].Prev = subTours[3].Structured[1];
            }

            if (subTours[3].Structured[1].Prev.Equals(subTours[3].Structured[0]))
            {
                subTours[3].Structured[1].Prev = subTours[2].Structured[3];
                subTours[3].Structured[0].Next = subTours[1].Structured[5];
            }
            else
            {
                subTours[3].Structured[1].Next = subTours[2].Structured[3];
                subTours[3].Structured[0].Prev = subTours[1].Structured[5];
            }

            if (subTours[1].Structured[5].Prev.Equals(subTours[1].Structured[4]))
            {
                subTours[1].Structured[5].Prev = subTours[3].Structured[0];
                subTours[1].Structured[4].Next = subTours[0].Structured[6];
            }
            else
            {
                subTours[1].Structured[5].Next = subTours[3].Structured[0];
                subTours[1].Structured[4].Prev = subTours[0].Structured[6];
            }

            //Update starting node
            subTours[0].List.StartNode = subTours[2].Structured[2];

            //Update structured references
            subTours[0].Structured[2] = subTours[1].Structured[2];
            subTours[0].Structured[3] = subTours[1].Structured[3];
            subTours[0].Structured[4] = subTours[2].Structured[4];
            subTours[0].Structured[5] = subTours[2].Structured[5];
            subTours[0].Structured[6] = subTours[3].Structured[6];
            subTours[0].Structured[7] = subTours[3].Structured[7];

            //Update list size
            subTours[0].List.Count = subTours[0].List.Count + subTours[1].List.Count + subTours[2].List.Count + subTours[3].List.Count;

            return subTours[0];
        }
    }
}
