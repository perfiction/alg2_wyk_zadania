﻿namespace KnightsTour
{
    partial class VisualizationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableAntialiasingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.highlightStartAndEndNodesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkerboardInsteadOfGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 24);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(500, 500);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.SizeChanged += new System.EventHandler(this.pictureBox_SizeChanged);
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(500, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enableAntialiasingToolStripMenuItem,
            this.highlightStartAndEndNodesToolStripMenuItem,
            this.checkerboardInsteadOfGridToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // enableAntialiasingToolStripMenuItem
            // 
            this.enableAntialiasingToolStripMenuItem.Checked = true;
            this.enableAntialiasingToolStripMenuItem.CheckOnClick = true;
            this.enableAntialiasingToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableAntialiasingToolStripMenuItem.Name = "enableAntialiasingToolStripMenuItem";
            this.enableAntialiasingToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.enableAntialiasingToolStripMenuItem.Text = "Enable anti-aliasing";
            this.enableAntialiasingToolStripMenuItem.Click += new System.EventHandler(this.settings_Click);
            // 
            // highlightStartAndEndNodesToolStripMenuItem
            // 
            this.highlightStartAndEndNodesToolStripMenuItem.Checked = true;
            this.highlightStartAndEndNodesToolStripMenuItem.CheckOnClick = true;
            this.highlightStartAndEndNodesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.highlightStartAndEndNodesToolStripMenuItem.Name = "highlightStartAndEndNodesToolStripMenuItem";
            this.highlightStartAndEndNodesToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.highlightStartAndEndNodesToolStripMenuItem.Text = "Highlight start and end nodes";
            this.highlightStartAndEndNodesToolStripMenuItem.Click += new System.EventHandler(this.settings_Click);
            // 
            // checkerboardInsteadOfGridToolStripMenuItem
            // 
            this.checkerboardInsteadOfGridToolStripMenuItem.CheckOnClick = true;
            this.checkerboardInsteadOfGridToolStripMenuItem.Name = "checkerboardInsteadOfGridToolStripMenuItem";
            this.checkerboardInsteadOfGridToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.checkerboardInsteadOfGridToolStripMenuItem.Text = "Show checkerboard instead of grid";
            this.checkerboardInsteadOfGridToolStripMenuItem.Click += new System.EventHandler(this.settings_Click);
            // 
            // VisualizationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 524);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "VisualizationForm";
            this.Text = "Closed knight\'s tour visualization";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VisualizationForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableAntialiasingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem highlightStartAndEndNodesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkerboardInsteadOfGridToolStripMenuItem;
    }
}