﻿using System.Collections.Generic;
using System.Drawing;

namespace KnightsTour.DataStructures
{
    public class Tour
    {
        public DoublyLinkedList<Point> List { get; set; }
        public List<Node<Point>> Structured { get; set; }

        public Tour()
        {
            List = new DoublyLinkedList<Point>();
            Structured = new List<Node<Point>>();
            for (int i = 0; i < 8; i++)
                Structured.Add(new Node<Point>());
        }

        public List<Point> ToList()
        {
            Node<Point> node = List.StartNode;
            node.Visited = true;

            List<Point> list = new List<Point>();
            list.Add(node.Value);

            while (!node.Next.Visited || !node.Prev.Visited)
            {
                node = !node.Next.Visited ? node.Next : node.Prev;

                list.Add(node.Value);
                node.Visited = true;
            }

            return list;
        }
    }
}
