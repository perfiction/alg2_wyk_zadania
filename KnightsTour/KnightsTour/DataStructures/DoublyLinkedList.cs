﻿using System;

namespace KnightsTour.DataStructures
{
    public class DoublyLinkedList<T>
    {
        public Node<T> Head { get; set; }
        public Node<T> Tail { get; set; }
        public Node<T> StartNode { get; set; }
        public int Count { get; set; }
        public bool IsEmpty => Head == null;

        public DoublyLinkedList()
        {
            Head = null;
            Tail = null;
            StartNode = null;
            Count = 0;
        }

        public void Add(T value)
        {
            Node<T> node = new Node<T>(value);

            if (IsEmpty)
            {
                node.Next = null;
                node.Prev = null;
                Head = node;
                Tail = node;
            }
            else
            {
                Tail.Next = node;
                node.Prev = Tail;
                node.Next = null;
                Tail = node;
            }

            Count++;
        }

        public void Remove()
        {
            if (IsEmpty) throw new IndexOutOfRangeException("List is empty");

            if (Tail.Prev == null)
            {
                Head = null;
                Tail = null;
            }
            else
            {
                Tail = Tail.Prev;
                Tail.Next = null;
            }

            Count--;
        }

        public void Remove(T value)
        {
            if (IsEmpty) throw new IndexOutOfRangeException("List is empty");

            Node<T> node = Head;
            while (node.Next != null && !node.Value.Equals(value))
            {
                node = node.Next;
            }

            node.Prev.Next = node.Next;
            if(node.Next != null)
                node.Next.Prev = node.Prev;
        }

        public void Clear()
        {
            Head = null;
            Tail = null;
            Count = 0;
        }
    }
}
