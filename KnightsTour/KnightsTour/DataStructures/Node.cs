﻿namespace KnightsTour.DataStructures
{
    public class Node<T>
    {
        public Node<T> Next { get; set; }
        public Node<T> Prev { get; set; }
        public T Value { get; set; }
        public bool Visited { get; set; }

        public Node(T value)
        {
            Value = value;
        }

        public Node() { }

        public override bool Equals(object obj)
        {
            Node<T> o = (Node<T>)obj;
            return obj != null && Value.Equals(o.Value);
        }
    }
}
