﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Windows.Forms;

namespace KnightsTour
{
    public partial class VisualizationForm : Form
    {
        private readonly List<Point> _tour;
        private List<Point> _pointsToDraw;
        private readonly (int width, int height) _boardSize;
        private readonly int _animationDelay;

        private static int _pointSize = 32;

        public VisualizationForm()
        {
            InitializeComponent();
        }

        public VisualizationForm(int animationDelay, (int width, int height) boardSize, List<Point> tour, int iterations)
        {
            InitializeComponent();
            _tour = tour;
            _boardSize = boardSize;
            _animationDelay = animationDelay;

            _pointSize = (int)(Math.Min(pictureBox.Width, pictureBox.Height) / (2f * Math.Max(_boardSize.width, _boardSize.height)));
            _pointsToDraw = new List<Point>();

            this.Text += " (iterations: " + iterations + ")";

            backgroundWorker.RunWorkerAsync();
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = enableAntialiasingToolStripMenuItem.Checked ? SmoothingMode.HighQuality : SmoothingMode.None;

            // Draw grid or checkerboard
            if (checkerboardInsteadOfGridToolStripMenuItem.Checked)
                DrawCheckerboard(e.Graphics);
            else
                DrawGrid(e.Graphics);

            // Draw lines
            if (_pointsToDraw != null)
                DrawLinesAndPoints(e.Graphics);
        }

        private void DrawGrid(Graphics g)
        {
            float dx = pictureBox.Width / (float)_boardSize.width;
            float dy = pictureBox.Height / (float)_boardSize.height;

            for (int y = 0; y < _boardSize.height; y++)
                g.DrawLine(Pens.Black, 0, y * dy, pictureBox.Width, y * dy);
            for (int x = 0; x < _boardSize.width; x++)
                g.DrawLine(Pens.Black, x * dx, 0, x * dx, pictureBox.Height);
        }

        private void DrawCheckerboard(Graphics g)
        {
            float dx = pictureBox.Width / (float)_boardSize.width;
            float dy = pictureBox.Height / (float)_boardSize.height;
            bool color = true;

            for (int y = 0; y < _boardSize.height; y++)
            {
                color = !color;
                for (int x = 0; x < _boardSize.width; x++)
                {
                    var brush = color ? Brushes.DarkGray : Brushes.White;
                    RectangleF rect = new RectangleF(x * dx, y * dy, dx, dy);
                    g.FillRectangle(brush, rect);
                    color = !color;
                }
            }
        }

        private void DrawLinesAndPoints(Graphics g)
        {
            float dx = pictureBox.Width / (float)_boardSize.width;
            float dy = pictureBox.Height / (float)_boardSize.height;

            if (_pointsToDraw.Count == _tour.Count)
            {
                Point from = _pointsToDraw[0];
                Point to = _pointsToDraw[_pointsToDraw.Count - 1];
                float x1 = from.X * dx - _pointSize / 2f + (dx / 2f);
                float y1 = from.Y * dy - _pointSize / 2f + (dy / 2f);
                float x2 = to.X * dx - _pointSize / 2f + (dx / 2f);
                float y2 = to.Y * dy - _pointSize / 2f + (dy / 2f);
                g.DrawLine(Pens.Black, x1 + _pointSize / 2f, y1 + _pointSize / 2f, x2 + _pointSize / 2f, y2 + _pointSize / 2f);
            }

            for (int i = 0; i < _pointsToDraw.Count; i++)
            {
                Point from = _pointsToDraw[i];
                float x1 = from.X * dx - _pointSize / 2f + (dx / 2f);
                float y1 = from.Y * dy - _pointSize / 2f + (dy / 2f);

                if (i + 1 < _pointsToDraw.Count)
                {
                    Point to = _pointsToDraw[i + 1];
                    float x2 = to.X * dx - _pointSize / 2f + (dx / 2f);
                    float y2 = to.Y * dy - _pointSize / 2f + (dy / 2f);

                    g.DrawLine(Pens.Black, x1 + _pointSize / 2f, y1 + _pointSize / 2f, x2 + _pointSize / 2f, y2 + _pointSize / 2f);
                    g.FillEllipse(Brushes.Black, x2, y2, _pointSize, _pointSize);
                }

                if (i == 0 && highlightStartAndEndNodesToolStripMenuItem.Checked)
                    g.FillEllipse(Brushes.Red, x1, y1, _pointSize, _pointSize);
                else if (i == _tour.Count - 1 && highlightStartAndEndNodesToolStripMenuItem.Checked)
                    g.FillEllipse(Brushes.Blue, x1, y1, _pointSize, _pointSize);
                else
                    g.FillEllipse(Brushes.Black, x1, y1, _pointSize, _pointSize);
            }
        }

        private void pictureBox_SizeChanged(object sender, EventArgs e)
        {
            _pointSize = (int)(Math.Min(pictureBox.Width, pictureBox.Height) / (2f * Math.Max(_boardSize.width, _boardSize.height)));
            pictureBox.Invalidate();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if(_animationDelay != 0)
                Thread.Sleep(500);

            if (_animationDelay == 0)
            {
                _pointsToDraw = _tour;

                if (pictureBox.InvokeRequired)
                    pictureBox.Invoke(new Action(() => pictureBox.Invalidate()));
                else
                    pictureBox.Invalidate();
            }
            else
            {
                foreach (var p in _tour)
                {
                    if (e.Cancel) break;

                    _pointsToDraw.Add(p);
                    pictureBox.Invalidate();
                    Thread.Sleep(_animationDelay);
                }
            }
        }

        private void VisualizationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            backgroundWorker.CancelAsync();
        }

        private void settings_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker.IsBusy)
                pictureBox.Invalidate();
        }
    }
}
